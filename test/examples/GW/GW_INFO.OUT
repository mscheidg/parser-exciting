
================================================================================
=                            Main GW output file                               =
================================================================================


********************************************************************************
*                            GW input parameters                               *
********************************************************************************


 GW taskname:

   g0w0 - G0W0 run

--------------------------------------------------------------------------------

 Frequency integration parameters:
 Number of frequencies:           16
 Cutoff frequency:    1.0000000000000000     
 Grid type:
   gaule2 - Grid for double Gauss-Legendre quadrature,
            from 0 to freqmax and from freqmax to infinity
 Convolution method:
   imfreq : weights calculated for imaginary frequecies

--------------------------------------------------------------------------------

 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Serence, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by S. Massidda, M. Posternak, and A. Baldereschi, PRB 48, 5058 (1993)

--------------------------------------------------------------------------------

 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:    1.0000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):    1.0000000000000000     

--------------------------------------------------------------------------------

 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb):    2.0000000000000000     
   Error tolerance for structure constants:    1.0000000000000001E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.10000000000000001     

--------------------------------------------------------------------------------

 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000

--------------------------------------------------------------------------------

 Core electrons treatment:
   all - Core states are included in all calculations

--------------------------------------------------------------------------------

 Number of empty states (GW):           22
 Quasiparticle band range:       1     10

   k/q-points grid:            2           2           2

--------------------------------------------------------------------------------
-                                 k-vectors                                    -
--------------------------------------------------------------------------------

 Total number of k-points: < nkptnr >           8
 Symmetry reduced number of k-points: < nkpt >           3
 k-vectors list: < ik    vkl    vkc    weight >
   1      0.0000  0.0000  0.0000      0.0000  0.0000  0.0000      0.1250
   2      0.0000  0.0000  0.5000     -0.3062  0.3062  0.3062      0.5000
   3      0.0000  0.5000  0.5000      0.0000  0.0000  0.6124      0.3750
 Mapping from non-reduced to reduced grid: < ik2ikp >
           1           2           2           3           2           3           3           2
 Mapping from reduced to non-reduced grid: < ikp2ik >
           1           2           4

 Tetrahedron method info: < ntet    tvol >
     2  0.20833333E-01
 Nodal points of tetrahedron: < itet    tnodes    wtet >
           1           1           2           2           3          24
           2           2           2           3           3          24

--------------------------------------------------------------------------------
-                           Mixed product WF info                              -
--------------------------------------------------------------------------------

  Maximal number of MT wavefunctions per atom:          159
  Total number of MT wavefunctions:                     318
  Maximal number of PW wavefunctions:                   174
  Total number of mixed-product wavefunctions:          492


--------------------------------------------------------------------------------
-                      Kohn-Sham bandstructure analysis                        -
--------------------------------------------------------------------------------

 Fermi energy:     0.2012
 Energy range:    -0.2497    1.4779
 Band index of VBM:   4
 Band index of CBM:   5

 Fundamental BandGap (eV):                 0.5813
 at k(VBM) =    0.000   0.000   0.000 ik =     1
    k(CBM) =    0.000   0.500   0.500 ik =     3
 Optical BandGap (eV):                     2.5100

--------------------------------------------------------------------------------

 Maximum number of LAPW states:                      182
 Minimal number of LAPW states:                      176
 Number of states used in GW:
     - total KS                                       27
     - occupied                                        4
     - unoccupied                                     22
     - dielectric function                            27
     - self energy                                    27
 Energy of the highest unoccupied state:        1.477909
 Number of valence electrons:                          8
 Number of valence electrons treated in GW:            8

--------------------------------------------------------------------------------
-                               frequency grid                                 -
--------------------------------------------------------------------------------

 Type: < fgrid >
   - Grid for double Gauss-Legendre quadrature,
     from 0 to freqmax and from freqmax to infinity
 Convolution method: < fconv >
   - imfreq : weights calculated for imaginary frequecies
 Number of frequencies: < nomeg >          16
 Cutoff frequency: < freqmax >   1.0000000000000000     
 frequency list: < #    freqs    weight >
   1  1.9855071751E-02  5.0614268145E-02
   2  0.1016667613      0.1111905172    
   3  0.2372337950      0.1568533229    
   4  0.4082826788      0.1813418917    
   5  0.5917173212      0.1813418917    
   6  0.7627662050      0.1568533229    
   7  0.8983332387      0.1111905172    
   8  0.9801449282      5.0614268145E-02
   9   1.020257282      5.2685653046E-02
  10   1.113172659      0.1377821040    
  11   1.311017706      0.2695943819    
  12   1.689996159      0.5179282224    
  13   2.449283430       1.087868072    
  14   4.215251035       2.787023374    
  15   9.836056419       10.75746081    
  16   50.36496531       128.3896574    

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-                  Peak memory estimate (Mb, per process):                     -
--------------------------------------------------------------------------------

 Global data:                              0.26
 <n|Vxc|n>:                                0.00
 BZ integration weights:                   0.00
 PW-IPW overlap matrix:                    3.62
 Coulomb potential:                        3.70
 KS eigenvectors:                          0.42
 Minm:                                     5.27
 Momentum matrix elements:                 0.05
 (q,omega)-BZ integration weights:         1.85
 Dielectric function:                     59.46
 M*W*M:                                    0.07
 Self-energy:                              0.02
 _______________________________________________
 Total G0W0:                              77.47

--------------------------------------------------------------------------------


================================================================================
=                                  GW cycle                                    =
================================================================================

 - Maximum singular eigenvector ###
immax, max(wi0new), barcev(immax):    1       0.989     0.000817
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         487
  - New basis set size =         486
 - Maximum singular eigenvector ###
immax, max(wi0new), barcev(immax):    1       0.989     0.000817
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         487
  - New basis set size =         438
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         486
  - New basis set size =         442
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         486
  - New basis set size =         442
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         492
  - New basis set size =         444
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         486
  - New basis set size =         442
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         492
  - New basis set size =         444
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         492
  - New basis set size =         444
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =         486
  - New basis set size =         442

--------------------------------------------------------------------------------
-                                     KS                                       -
--------------------------------------------------------------------------------

 Fermi energy:     0.2012
 Energy range:    -0.2497    0.5930
 Band index of VBM:   4
 Band index of CBM:   5

 Fundamental BandGap (eV):                 0.5813
 at k(VBM) =    0.000   0.000   0.000 ik =     1
    k(CBM) =    0.000   0.500   0.500 ik =     3
 Optical BandGap (eV):                     2.5100

--------------------------------------------------------------------------------


 Perform perturbative G0W0 without energy shift


--------------------------------------------------------------------------------
-                                    G0W0                                      -
--------------------------------------------------------------------------------

 Fermi energy:     0.0150
 Energy range:    -0.4643    0.4351
 Band index of VBM:   4
 Band index of CBM:   5

 Fundamental BandGap (eV):                 1.2734
 at k(VBM) =    0.000   0.000   0.000 ik =     1
    k(CBM) =    0.000   0.500   0.500 ik =     3
 Optical BandGap (eV):                     3.3477

--------------------------------------------------------------------------------


================================================================================
=                          GW timing info (seconds)                            =
================================================================================

 Initialization                             :         3.29
     - init_scf                             :         2.97
     - init_kpt                             :         0.09
     - init_eval                            :         0.00
     - init_freq                            :         0.00
     - init_mb                              :         0.22
 Subroutines                                : 
     - calcpmat                             :         1.77
     - calcbarcmb                           :        11.52
     - BZ integration weights               :         0.79
     Dielectric function                    :       205.92
     - head                                 :         0.01
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :        22.13
     WF products expansion                  :         0.88
     - diagsgi                              :         0.26
     - calcmpwipw                           :         0.62
     - calcmicm                             :         0.09
     - calcminc                             :         0.04
     - calcminm                             :        14.51
     Self-energy                            :        67.34
     - calcselfx                            :         5.14
     - calcselfc                            :        62.21
     - calcvxcnn                            :         0.48
     - input/output                         :         0.03
 _________________________________________________________
 Total                                      :       313.41


================================================================================
=                            Main GW output file                               =
================================================================================


********************************************************************************
*                            GW input parameters                               *
********************************************************************************


 GW taskname:

   band - Calculate QP bandstructure

--------------------------------------------------------------------------------

 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Serence, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by S. Massidda, M. Posternak, and A. Baldereschi, PRB 48, 5058 (1993)

--------------------------------------------------------------------------------

 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:    1.0000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):    1.0000000000000000     

--------------------------------------------------------------------------------

 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb):    2.0000000000000000     
   Error tolerance for structure constants:    1.0000000000000001E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.10000000000000001     

--------------------------------------------------------------------------------

 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000

--------------------------------------------------------------------------------

 Core electrons treatment:
   all - Core states are included in all calculations

--------------------------------------------------------------------------------

 Number of empty states (GW):           22
 Quasiparticle band range:       1     10

   k/q-points grid:            2           2           2

================================================================================
=                          GW timing info (seconds)                            =
================================================================================

 Initialization                             :         0.00
     - init_scf                             :         0.00
     - init_kpt                             :         0.00
     - init_eval                            :         0.00
     - init_freq                            :         0.00
     - init_mb                              :         0.00
 Subroutines                                : 
     - calcpmat                             :         0.00
     - calcbarcmb                           :         0.00
     - BZ integration weights               :         0.00
     Dielectric function                    :         0.00
     - head                                 :         0.00
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :         0.00
     WF products expansion                  :         0.00
     - diagsgi                              :         0.00
     - calcmpwipw                           :         0.00
     - calcmicm                             :         0.00
     - calcminc                             :         0.00
     - calcminm                             :         0.00
     Self-energy                            :         0.00
     - calcselfx                            :         0.00
     - calcselfc                            :         0.00
     - calcvxcnn                            :         0.00
     - input/output                         :         0.00
 _________________________________________________________
 Total                                      :         0.37


================================================================================
=                            Main GW output file                               =
================================================================================


********************************************************************************
*                            GW input parameters                               *
********************************************************************************


 GW taskname:

   dos - Calculate QP DOS

--------------------------------------------------------------------------------

 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Serence, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by S. Massidda, M. Posternak, and A. Baldereschi, PRB 48, 5058 (1993)

--------------------------------------------------------------------------------

 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:    1.0000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):    1.0000000000000000     

--------------------------------------------------------------------------------

 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb):    2.0000000000000000     
   Error tolerance for structure constants:    1.0000000000000001E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.10000000000000001     

--------------------------------------------------------------------------------

 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000

--------------------------------------------------------------------------------

 Core electrons treatment:
   all - Core states are included in all calculations

--------------------------------------------------------------------------------

 Number of empty states (GW):           22
 Quasiparticle band range:       1     10

   k/q-points grid:            2           2           2

================================================================================
=                          GW timing info (seconds)                            =
================================================================================

 Initialization                             :         0.00
     - init_scf                             :         0.00
     - init_kpt                             :         0.00
     - init_eval                            :         0.00
     - init_freq                            :         0.00
     - init_mb                              :         0.00
 Subroutines                                : 
     - calcpmat                             :         0.00
     - calcbarcmb                           :         0.00
     - BZ integration weights               :         0.00
     Dielectric function                    :         0.00
     - head                                 :         0.00
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :         0.00
     WF products expansion                  :         0.00
     - diagsgi                              :         0.00
     - calcmpwipw                           :         0.00
     - calcmicm                             :         0.00
     - calcminc                             :         0.00
     - calcminm                             :         0.00
     Self-energy                            :         0.00
     - calcselfx                            :         0.00
     - calcselfc                            :         0.00
     - calcvxcnn                            :         0.00
     - input/output                         :         0.00
 _________________________________________________________
 Total                                      :         0.43

