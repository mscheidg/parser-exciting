 
================================================================================
=                            Main GW output file                               =
================================================================================
 
 
********************************************************************************
*                            GW input parameters                               *
********************************************************************************
 
 
 GW taskname:
 
   g0w0 - G0W0 run
 
--------------------------------------------------------------------------------
 
 Frequency integration parameters:
 Number of frequencies:           16
 Cutoff frequency:   0.500000000000000     
 Grid type:
   gaule2 - Grid for double Gauss-Legendre quadrature,
            from 0 to freqmax and from freqmax to infinity
 Convolution method:
   imfreq : weights calculated for imaginary frequecies
 
--------------------------------------------------------------------------------
 
 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Seren
 ce, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by P. Carrier, S. Rohra, and A. Goerling, PRB 75, 205
 126 (2007)
 
--------------------------------------------------------------------------------
 
 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:   1.000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):   0.800000000000000     
 
--------------------------------------------------------------------------------
 
 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb): 
   4.00000000000000     
   Error tolerance for structure constants:   1.000000000000000E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.300000000000000     
   Slab (2d) cutoff is applied (vacuum along z-axis)
 
--------------------------------------------------------------------------------
 
 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000
 
--------------------------------------------------------------------------------
 
 Core electrons treatment:
   all - Core states are included in all calculations
 
--------------------------------------------------------------------------------
 
 Number of empty states (GW):         1000
 Quasiparticle band range:       7     10
 
 k/q-points grid:            9          15           1
 
  Matrix block size:        10000
 
--------------------------------------------------------------------------------
-                           Mixed product WF info                              -
--------------------------------------------------------------------------------
 
  Maximal number of MT wavefunctions per atom:          139
  Total number of MT wavefunctions:                     556
  Maximal number of PW wavefunctions:                  1522
  Total number of mixed-product wavefunctions:         2078
 
 
--------------------------------------------------------------------------------
-                      Kohn-Sham bandstructure analysis                        -
--------------------------------------------------------------------------------
 
 Fermi energy:    -0.0677
 Energy range:    -0.7995    6.1760
 Band index of VBM:   8
 Band index of CBM:   9
 
 Direct BandGap (eV):                      4.6505
 at k      =    0.000   0.333   0.000 ik =     6
 Optical BandGap (eV):                     5.5656
 
--------------------------------------------------------------------------------
 
 Maximum number of LAPW states:                     2950
 Minimal number of LAPW states:                     2882
 Number of states used in GW:
     - total KS                                     1009
     - occupied                                        8
     - unoccupied                                   1000
     - dielectric function                          1009
     - self energy                                  1009
 Energy of the highest unoccupied state:        6.175988
 Number of valence electrons:                         16
 Number of valence electrons treated in GW:            4
 
--------------------------------------------------------------------------------
-                               frequency grid                                 -
--------------------------------------------------------------------------------
 
 Type: < fgrid >
   - Grid for double Gauss-Legendre quadrature,
     from 0 to freqmax and from freqmax to infinity
 Convolution method: < fconv >
   - imfreq : weights calculated for imaginary frequecies
 Number of frequencies: < nomeg >          16
 Cutoff frequency: < freqmax >  0.500000000000000     
 frequency list: < #    freqs    weight >
   1  9.9275358756E-03  2.5307134073E-02
   2  5.0833380647E-02  5.5595258613E-02
   3  0.1186168975      7.8426661469E-02
   4  0.2041413394      9.0670945845E-02
   5  0.2958586606      9.0670945845E-02
   6  0.3813831025      7.8426661469E-02
   7  0.4491666194      5.5595258613E-02
   8  0.4900724641      2.5307134073E-02
   9  0.5101286408      2.6342826523E-02
  10  0.5565863295      6.8891051997E-02
  11  0.6555088528      0.1347971910    
  12  0.8449980794      0.2589641112    
  13   1.224641715      0.5439340360    
  14   2.107625517       1.393511687    
  15   4.918028209       5.378730407    
  16   25.18248266       64.19482869    
 
--------------------------------------------------------------------------------
 
 
--------------------------------------------------------------------------------
-                  Peak memory estimate (Mb, per process):                     -
--------------------------------------------------------------------------------
 
 Global data:                              0.26
 <n|Vxc|n>:                                2.08
 BZ integration weights:                   2.08
 PW-IPW overlap matrix:                 2205.73
 Coulomb potential:                       65.90
 KS eigenvectors:                        110.79
 Minm:                                   255.95
 Momentum matrix elements:                46.79
 (q,omega)-BZ integration weights:     ********
 Dielectric function:                   1055.74
 M*W*M:                                    0.99
 Self-energy:                              0.16
 _______________________________________________
 Total G0W0:                            3813.75
 
--------------------------------------------------------------------------------
 
 
================================================================================
=                                  GW cycle                                    =
================================================================================
 
 
--------------------------------------------------------------------------------
-                                     KS                                       -
--------------------------------------------------------------------------------
 
 Fermi energy:    -0.0677
 Energy range:    -0.2774    0.1183
 Band index of VBM:   8
 Band index of CBM:   9
 
 Direct BandGap (eV):                      4.6505
 at k      =    0.000   0.333   0.000 ik =     6
 Optical BandGap (eV):                     5.5656
 
--------------------------------------------------------------------------------
 
 
 Perform perturbative G0W0 without energy shift
 
  # iter    Ef_QP    es    Eg/eV    Eg(k=0)/eV)
   0   -0.013276    0.000000    0.255271    0.291921
 
--------------------------------------------------------------------------------
-                                    G0W0                                      -
--------------------------------------------------------------------------------
 
 Fermi energy:    -0.0133
 Energy range:    -0.2818    0.2391
 Band index of VBM:   8
 Band index of CBM:   9
 
 Fundamental BandGap (eV):                 6.9463
 at k(VBM) =    0.000   0.667   0.000 ik =    11
    k(CBM) =    0.000   0.000   0.000 ik =     1
 Optical BandGap (eV):                     7.9436
 
--------------------------------------------------------------------------------
 
 
================================================================================
=                          GW timing info (seconds)                            =
================================================================================
 
 Initialization                             :       286.89
     - init_scf                             :       234.68
     - init_kpt                             :        34.27
     - init_eval                            :         0.19
     - init_freq                            :         0.01
     - init_mb                              :        17.68
 Subroutines                                : 
     - calcpmat                             :        41.24
     - calcbarcmb                           :       591.26
     - BZ integration weights               :       226.58
     Dielectric function                    :    272552.73
     - head                                 :         2.94
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :       160.97
     WF products expansion                  :       252.19
     - diagsgi                              :         9.14
     - calcmpwipw                           :       243.04
     - calcmicm                             :         9.61
     - calcminc                             :         1.44
     - calcminm                             :    355050.47
     Self-energy                            :    256396.76
     - calcselfx                            :    107380.15
     - calcselfc                            :    149016.61
     - calcvxcnn                            :        21.22
     - input/output                         :       159.86
 _________________________________________________________
 Total                                      :    566989.33
 
 
================================================================================
=                            Main GW output file                               =
================================================================================
 
 
********************************************************************************
*                            GW input parameters                               *
********************************************************************************
 
 
 GW taskname:
 
   band - Calculate QP bandstructure
 
--------------------------------------------------------------------------------
 
 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Seren
 ce, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by P. Carrier, S. Rohra, and A. Goerling, PRB 75, 205
 126 (2007)
 
--------------------------------------------------------------------------------
 
 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:   1.000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):   0.800000000000000     
 
--------------------------------------------------------------------------------
 
 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb): 
   4.00000000000000     
   Error tolerance for structure constants:   1.000000000000000E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.300000000000000     
   Slab (2d) cutoff is applied (vacuum along z-axis)
 
--------------------------------------------------------------------------------
 
 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000
 
--------------------------------------------------------------------------------
 
 Core electrons treatment:
   all - Core states are included in all calculations
 
--------------------------------------------------------------------------------
 
 Number of empty states (GW):         1000
 Quasiparticle band range:       7     10
 
 k/q-points grid:            9          15           1
 
  Matrix block size:        10000
 
================================================================================
=                          GW timing info (seconds)                            =
================================================================================
 
 Initialization                             :         0.00
     - init_scf                             :         0.00
     - init_kpt                             :         0.00
     - init_eval                            :         0.00
     - init_freq                            :         0.00
     - init_mb                              :         0.00
 Subroutines                                : 
     - calcpmat                             :         0.00
     - calcbarcmb                           :         0.00
     - BZ integration weights               :         0.00
     Dielectric function                    :         0.00
     - head                                 :         0.00
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :         0.00
     WF products expansion                  :         0.00
     - diagsgi                              :         0.00
     - calcmpwipw                           :         0.00
     - calcmicm                             :         0.00
     - calcminc                             :         0.00
     - calcminm                             :         0.00
     Self-energy                            :         0.00
     - calcselfx                            :         0.00
     - calcselfc                            :         0.00
     - calcvxcnn                            :         0.00
     - input/output                         :         0.00
 _________________________________________________________
 Total                                      :         0.25
 
 
================================================================================
=                            Main GW output file                               =
================================================================================
 
 
********************************************************************************
*                            GW input parameters                               *
********************************************************************************
 
 
 GW taskname:
 
   band - Calculate QP bandstructure
 
--------------------------------------------------------------------------------
 
 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Seren
 ce, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by P. Carrier, S. Rohra, and A. Goerling, PRB 75, 205
 126 (2007)
 
--------------------------------------------------------------------------------
 
 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:   1.000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):   0.800000000000000     
 
--------------------------------------------------------------------------------
 
 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb): 
   4.00000000000000     
   Error tolerance for structure constants:   1.000000000000000E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.300000000000000     
   Slab (2d) cutoff is applied (vacuum along z-axis)
 
--------------------------------------------------------------------------------
 
 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000
 
--------------------------------------------------------------------------------
 
 Core electrons treatment:
   all - Core states are included in all calculations
 
--------------------------------------------------------------------------------
 
 Number of empty states (GW):         1000
 Quasiparticle band range:       7     10
 
 k/q-points grid:            9          15           1
 
  Matrix block size:        10000
 
================================================================================
=                          GW timing info (seconds)                            =
================================================================================
 
 Initialization                             :         0.00
     - init_scf                             :         0.00
     - init_kpt                             :         0.00
     - init_eval                            :         0.00
     - init_freq                            :         0.00
     - init_mb                              :         0.00
 Subroutines                                : 
     - calcpmat                             :         0.00
     - calcbarcmb                           :         0.00
     - BZ integration weights               :         0.00
     Dielectric function                    :         0.00
     - head                                 :         0.00
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :         0.00
     WF products expansion                  :         0.00
     - diagsgi                              :         0.00
     - calcmpwipw                           :         0.00
     - calcmicm                             :         0.00
     - calcminc                             :         0.00
     - calcminm                             :         0.00
     Self-energy                            :         0.00
     - calcselfx                            :         0.00
     - calcselfc                            :         0.00
     - calcvxcnn                            :         0.00
     - input/output                         :         0.00
 _________________________________________________________
 Total                                      :         0.61
 
 
================================================================================
=                            Main GW output file                               =
================================================================================
 
 
********************************************************************************
*                            GW input parameters                               *
********************************************************************************
 
 
 GW taskname:
 
   emac - Calculate the DFT frequency-dependent macroscopic dielectric function 
 for q=0
 
--------------------------------------------------------------------------------
 
 Frequency integration parameters:
 Number of frequencies:           16
 Cutoff frequency:   0.500000000000000     
 Grid type:
   gaule2 - Grid for double Gauss-Legendre quadrature,
            from 0 to freqmax and from freqmax to infinity
 Convolution method:
   imfreq : weights calculated for imaginary frequecies
 
--------------------------------------------------------------------------------
 
 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative G0W0 without energy shift
 Analytic continuation method:
  pade - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Seren
 ce, J. Low Temp. Phys. 29, 179 (1977))
 Number of poles used in analytic continuation:           8
 Scheme to treat singularities:
 Auxiliary function method by P. Carrier, S. Rohra, and A. Goerling, PRB 75, 205
 126 (2007)
 
--------------------------------------------------------------------------------
 
 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:   1.000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):   0.800000000000000     
 
--------------------------------------------------------------------------------
 
 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb): 
   4.00000000000000     
   Error tolerance for structure constants:   1.000000000000000E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.300000000000000     
   Slab (2d) cutoff is applied (vacuum along z-axis)
 
--------------------------------------------------------------------------------
 
 Screened Coulomb potential parameters:
   Type of averaging: isotropic
  Averaging direction:   1.0000  1.0000  1.0000
 
--------------------------------------------------------------------------------
 
 Core electrons treatment:
   all - Core states are included in all calculations
 
--------------------------------------------------------------------------------
 
 Number of empty states (GW):         1000
 Quasiparticle band range:       7     10
 
 k/q-points grid:            9          15           1
 
  Matrix block size:        10000
 
================================================================================
=               Calculate the macroscopic dielectric function                  =
================================================================================
 
 
--------------------------------------------------------------------------------
-                           Mixed product WF info                              -
--------------------------------------------------------------------------------
 
  Maximal number of MT wavefunctions per atom:          139
  Total number of MT wavefunctions:                     556
  Maximal number of PW wavefunctions:                  1522
  Total number of mixed-product wavefunctions:         2078
 
 
--------------------------------------------------------------------------------
-                      Kohn-Sham bandstructure analysis                        -
--------------------------------------------------------------------------------
 
 Fermi energy:    -0.0677
 Energy range:    -0.7995    6.1760
 Band index of VBM:   8
 Band index of CBM:   9
 
 Direct BandGap (eV):                      4.6505
 at k      =    0.000   0.333   0.000 ik =     6
 Optical BandGap (eV):                     5.5656
 
--------------------------------------------------------------------------------
 
 Maximum number of LAPW states:                     2950
 Minimal number of LAPW states:                     2882
 Number of states used in GW:
     - total KS                                     1009
     - occupied                                        8
     - unoccupied                                   1000
     - dielectric function                          1009
     - self energy                                  1009
 Energy of the highest unoccupied state:        6.175988
 Number of valence electrons:                         16
 Number of valence electrons treated in GW:            4
 
--------------------------------------------------------------------------------
-                               frequency grid                                 -
--------------------------------------------------------------------------------
 
 Type: < fgrid >
   - Grid for double Gauss-Legendre quadrature,
     from 0 to freqmax and from freqmax to infinity
 Convolution method: < fconv >
   - imfreq : weights calculated for imaginary frequecies
 Number of frequencies: < nomeg >          16
 Cutoff frequency: < freqmax >  0.500000000000000     
 frequency list: < #    freqs    weight >
   1  9.9275358756E-03  2.5307134073E-02
   2  5.0833380647E-02  5.5595258613E-02
   3  0.1186168975      7.8426661469E-02
   4  0.2041413394      9.0670945845E-02
   5  0.2958586606      9.0670945845E-02
   6  0.3813831025      7.8426661469E-02
   7  0.4491666194      5.5595258613E-02
   8  0.4900724641      2.5307134073E-02
   9  0.5101286408      2.6342826523E-02
  10  0.5565863295      6.8891051997E-02
  11  0.6555088528      0.1347971910    
  12  0.8449980794      0.2589641112    
  13   1.224641715      0.5439340360    
  14   2.107625517       1.393511687    
  15   4.918028209       5.378730407    
  16   25.18248266       64.19482869    
 
--------------------------------------------------------------------------------
 
 
--------------------------------------------------------------------------------
-                  Peak memory estimate (Mb, per process):                     -
--------------------------------------------------------------------------------
 
 Global data:                              0.26
 <n|Vxc|n>:                                2.08
 BZ integration weights:                   2.08
 PW-IPW overlap matrix:                 2205.73
 Coulomb potential:                       65.90
 KS eigenvectors:                        110.79
 Minm:                                   255.95
 Momentum matrix elements:                46.79
 (q,omega)-BZ integration weights:     ********
 Dielectric function:                   1055.74
 M*W*M:                                    0.99
 Self-energy:                              0.16
 _______________________________________________
 Total G0W0:                            3813.75
 
--------------------------------------------------------------------------------
 
 - Maximum singular eigenvector ###
immax, max(wi0new), barcev(immax): 1489       0.122     0.000000
 Info(setbarcev): Product basis size has been changed
  - Old basis set size =        1977
  - New basis set size =           0
