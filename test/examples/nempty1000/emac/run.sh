#!/bin/bash
#
##Job name
#PBS -N rBN-G0W0-emac
##number of nodes and limit of the execution time (hours:mimutes:seconds)
#PBS -l nodes=1:ppn=9
#PBS -l walltime=720:00:00
##type of the queue (smallRam or bigRam)
#PBS -q bigRam
#PBS -m ae 
#PBS -M qiang@physik.hu-berlin.de
#PBS -W x=NACCESSPOLICY:SINGLEJOB

cd $PBS_O_WORKDIR
export PATH=$PATH:/app/intel/bin/:/app/intel/impi/4.0.3.008/intel64/bin/
export LD_LIBRARY_PATH=/app/intel/lib/intel64/:/app/intel/mkl/lib/intel64/:/app/intel/impi/4.0.3.008/intel64/lib/
export OMP_NUM_THREADS=3
ulimit -s unlimited

date
mpirun -n 9 -perhost 9 excitingmpi-gwmod-0717 
date
